require 'healthie/commands/cart'

RSpec.describe Healthie::Commands::Cart do
  it "executes `cart` command successfully" do
    output = StringIO.new
    options = {}
    command = Healthie::Commands::Cart.new(options)

    command.execute(output: output)

    expect(output.string).to include("Shopping list")
  end
end
