require 'healthie/actions/export_shopping_list'
require 'healthie/services/provisions/service'

RSpec.describe Healthie do
  describe "as a provisioner" do
    it "includes products without stock in the shopping list" do
      meal_plan = :regular

      shopping_list = Healthie::Action::ExportShoppingList.do(meal_plan)

      expected_products = Healthie::Service::Provisions.without_stock
      expected_products.each do |expected_product|
        expect(shopping_list).to include(expected_product[:name])
      end
    end

    it "does not include products with stock in the shopping list" do
      meal_plan = :regular

      shopping_list = Healthie::Action::ExportShoppingList.do(meal_plan)

      unexpected_products = Healthie::Service::Provisions.with_stock
      unexpected_products.each do |unexpected_product|
        expect(shopping_list).not_to include(unexpected_product[:name])
      end
    end
  end
end
