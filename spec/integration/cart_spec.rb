RSpec.describe "`healthie cart` command", type: :cli do
  it "executes `healthie help cart` command successfully" do
    output = `healthie help cart`
    expected_output = <<-OUT
Usage:
  healthie cart

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
