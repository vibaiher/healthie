# frozen_string_literal: true

require_relative '../command'
require_relative '../actions/export_shopping_list'

require 'tty-markdown'

module Healthie
  module Commands
    class Cart < Healthie::Command
      def initialize(options)
        @options = options
      end

      def execute(input: $stdin, output: $stdout)
        meal_plan = :regular
        shopping_list = Action::ExportShoppingList.do(meal_plan)
        markdown = TTY::Markdown.parse(shopping_list)

        output.puts markdown
      end
    end
  end
end
