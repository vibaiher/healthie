require 'json'

module Healthie
  module Service
    class Ingredients
      class << self
        def calculate_for(cookbooks)
          required_ingredients = {}
          ingredients = cookbooks.flat_map { |cookbook| cookbook[:ingredients] }
          ingredients.each do |ingredient|
            amount = ingredient.delete(:value)
            required_ingredients[ingredient[:name].to_sym] ||= collection.find { |i| i[:name] == ingredient[:name] }.merge(ingredient)
            required_ingredients[ingredient[:name].to_sym][:amount] ||= 0
            required_ingredients[ingredient[:name].to_sym][:amount] += amount.to_i
          end

          required_ingredients.values
        end

        private

        def collection
          @collection ||= fetch_collection
        end

        def fetch_collection
          collection_path = File.join(File.dirname(__FILE__), 'collection.json')
          content = File.read(collection_path)

          JSON.parse(content).map { |item| item.transform_keys(&:to_sym) }
        end
      end
    end
  end
end
