require 'json'

module Healthie
  module Service
    class Cookbooks
      class << self
        def all
          collection
        end

        private

        def collection
          @collection ||= fetch_collection
        end

        def fetch_collection
          collection_path = File.join(File.dirname(__FILE__), 'collection.json')
          content = File.read(collection_path)

          JSON.parse(content).map do |item|
            item = item.transform_keys(&:to_sym)
            ingredients = item[:ingredients].map { |value| value.transform_keys(&:to_sym) }
            item[:ingredients] = ingredients

            item
          end
        end
      end
    end
  end
end
