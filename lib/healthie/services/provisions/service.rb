require 'json'

module Healthie
  module Service
    class Provisions
      class << self
        def without_stock
          collection.reject { |provision| provision['stock'] }
        end

        def with_stock
          collection.select { |provision| provision['stock'] }
        end

        private

        def collection
          @collection ||= fetch_collection
        end

        def fetch_collection
          collection_path = File.join(File.dirname(__FILE__), 'collection.json')
          content = File.read(collection_path)

          JSON.parse(content).map { |item| item.transform_keys(&:to_sym) }
        end
      end
    end
  end
end
