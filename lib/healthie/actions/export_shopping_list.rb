require 'healthie/services/cookbooks/service'
require 'healthie/services/provisions/service'
require 'healthie/services/ingredients/service'

module Healthie
  module Action
    class ExportShoppingList
      class << self
        def do(meal_plan)
          cookbooks = obtain_cookbooks_from(meal_plan)
          ingredients = obtain_ingredients_from(cookbooks)
          buyables = detect_ingredients_without_stock(ingredients)
          write_shopping_list_with(buyables)
        end

        private

        def obtain_cookbooks_from(meal_plan)
          Service::Cookbooks.all
        end

        def obtain_ingredients_from(cookbooks)
          Service::Ingredients.calculate_for(cookbooks)
        end

        def detect_ingredients_without_stock(required_ingredients)
          ingredients_without_stock = Service::Provisions.without_stock.map { |ingredient| ingredient[:name] }

          required_ingredients.select do |ingredient|
            ingredients_without_stock.include?(ingredient[:name])
          end
        end

        def write_shopping_list_with(items)
          shopping_list = "# Shopping list\n"

          items.each do |item|
            shopping_list << "- #{item[:amount]} #{item[:unit]} de #{item[:name]} [](#{item[:product]})\n"
          end

          shopping_list
        end
      end
    end
  end
end
