# frozen_string_literal: true

require 'thor'

module Healthie
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'healthie version'
    def version
      require_relative 'version'
      puts "v#{Healthie::VERSION}"
    end
    map %w(--version -v) => :version

    desc 'cart', 'Command description...'
    method_option :help, aliases: '-h', type: :boolean,
                         desc: 'Display usage information'
    def cart(*)
      if options[:help]
        invoke :help, ['cart']
      else
        require_relative 'commands/cart'
        Healthie::Commands::Cart.new(options).execute
      end
    end
  end
end
